# Code Challenge - Feed List Display

In this assignment you will be using a simple JSON API to display a list of posts. It is not designed to be difficult, so be mindful of over-engineering your solution. There is no strict time limit for the project, but we would expect that it wouldn't take you more than a few hours to complete.

The design itself is flexible, though try to keep things clean, and where possible follow general Apple guidelines.

You are free to use 3rd party libraries within reason - example frameworks will generally be mentioned along with the requirements below.

## What we are looking for

- Coding style / readability
- Use of documentations / comments where necessary
- Understanding of Swift 
- Overall solution design
- Code maintainability, including testing
- Use of version control (preferably git)
- Any other techniques which would contribute to supportability of your code (e.g. dependency manager)

## Requirements

- latest version of Swift with latest Xcode
- Orientation fixed to portrait. iPad not required, but layout should look mostly correct on all iPhones
- The solution should show clear architecture pattern + separation of concerns, however the choice would be flexible
- Demonstrated unit tests, including mocking for any API requests (note that if the solution is following pattern that contains views, views portion needn't be unit tested)

### Bonus Points
Note that all of the following are optional, and the focus should definitely be on getting the main functionality and tests finished first.

- Pull-to-refresh feature
- View binding
- Image caching
- Data caching to improve subsequent load times
- Pagination is not required, however a simple explanation of how pagination might be implemented on both the frontend and backend would be great

We are not looking for specific implementation, however overall solution design is important. Feel free to use tools, code generators, templates, any latest and greatest api if that makes it easier.


## API endpoint details

GET Request:
`https://ios-code-challenge.mockservice.io/posts` (please note: mock service API is not available online)

Sample Response:

```
[  
   {  
      "userId":1,
      "id":1,
      "title":"sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      "body":"quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      "imageUrl":"https://via.placeholder.com/600/771796",
      "thumbnailUrl":"https://via.placeholder.com/150/771796"
   },
   {  
      "userId":1,
      "id":2,
      "title":"qui est esse",
      "body":"est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      "imageUrl":"https://via.placeholder.com/600/771796",
      "thumbnailUrl":"https://via.placeholder.com/150/771796"
   },
   {  
      "userId":1,
      "id":3,
      "title":"ea molestias quasi exercitationem repellat qui ipsa sit aut",
      "body":"et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
      "imageUrl":"https://via.placeholder.com/600/771796",
      "thumbnailUrl":"https://via.placeholder.com/150/771796"
   }
]
```
